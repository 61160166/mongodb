const mongoose = require('mongoose')
const User = require('./models/User')

mongoose.connect('mongodb://admin:password@localhost/mydb', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connection')
})

// eslint-disable-next-line array-callback-return
User.find((err, users) => {
  if (err) return console.err(err)
  console.log(users)
})
