const User = require('../models/User')
const userController = {
  userList: [
    { id: 1, name: 'Sarunporn', gender: 'F' },
    { id: 2, name: 'Sarunrat', gender: 'F' }
  ],
  lastId: 3,
  async addUser (req, res, next) {
    const payload = req.body
    console.log(payload)
    const users = new User(payload)
    try {
      const newuser = await users.save()
      res.json(newuser)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser (req, res, next) {
    const payload = req.body
    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    // res.json(usersController.deleteUser(id))
    try {
      const user = await User.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = userController
