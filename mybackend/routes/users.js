const express = require('express')
const router = express.Router()
const userController = require('../controller/UsersController')
const User = require('../models/User')

/* GET users listing. */
router.get('/', async (req, res, next) => {
  // res.json(usersController.getUsers())
  // Pattern 1
  // User.find({}).exec(function (err, user) {
  //   if (err) {
  //     res.status(500).send()
  //   }
  //   res.json(user)
  // })
  // Pattern 2 Promise
  // User.find({}).then(function (user) {
  //   res.json(user)
  // }).catch(function (err) {
  //   res.status(500).send(err)
  // })
  // Async Await
  try {
    const users = await User.find({})
    res.json(users)
  } catch (err) {
    res.status(500).send(err)
  }
})

router.get('/:id', async (req, res, next) => {
  const { id } = req.params
  // res.json(userController.getUsers(id))
  // User.findById(id).then(function (user) {
  //   res.json(user)
  // }).catch(function (err) {
  //   res.status(500).send(err)
  // })
  try {
    const users = await User.findById(id)
    res.json(users)
  } catch (err) {
    res.send(500).send(err)
  }
})

router.post('/', async (req, res, next) => {
  const payload = req.body
  // User.create(payload).then(function (user) {
  //   res.json(user)
  // }).catch(function (err) {
  //   res.send(500).send(err)
  // })
  console.log(payload)
  const users = new User(payload)
  try {
    const newuser = await users.save()
    res.json(newuser)
  } catch (err) {
    res.status(500).send(err)
  }
})

router.put('/', async (req, res, next) => {
  const payload = req.body
  // res.json(userController.updateUser(paylode))
  try {
    const users = await User.updateOne({ _id: payload._id }, payload)
    res.json(users)
  } catch (err) {
    res.status(500).send(err)
  }
})

router.delete('/:id', async (req, res, next) => {
  const { id } = req.params
  // res.json(userController.deleteUser(id))
  try {
    const users = await User.deleteOne({ _id: id })
    res.json(users)
  } catch (err) {
    res.status(500).send(err)
  }
})

module.exports = router
